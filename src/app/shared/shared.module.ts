import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StarsComponentComponent } from './stars-component/stars-component.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    StarsComponentComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    StarsComponentComponent,
    FormsModule,
    CommonModule
  ]
})
export class SharedModule { }
