import { Component, EventEmitter, Input, OnChanges, Output } from '@angular/core';

@Component({
  selector: 'jss-stars-component',
  templateUrl: './stars-component.component.html',
  styleUrls: ['./stars-component.component.css']
})
export class StarsComponentComponent implements OnChanges {


  @Output() clickStarRating: EventEmitter<any> = new EventEmitter<any>();

  
  @Input() rate = 0;
  cropWidth: number = 75;

  ngOnChanges(): void {
    if(this!=undefined){
      this.cropWidth = this.rate * 75/5;
    }
  }

  onClick(): void {
    this.clickStarRating.emit(this.rate)
  }

}
