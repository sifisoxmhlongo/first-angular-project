import { TestBed } from '@angular/core/testing';

import { ShoeDetailGuard } from './shoe-detail.guard';

describe('ShoeDetailGuard', () => {
  let guard: ShoeDetailGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ShoeDetailGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
