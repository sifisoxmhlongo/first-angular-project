import { Injectable } from "@angular/core";
import { IShoe } from "./IShoe";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable, map } from "rxjs";
import { catchError, tap, throwError } from "rxjs";

@Injectable({
    providedIn:'root'
})
export class ShoeService {

    private shoeUrl: string ='assets/shoes.json';

    constructor(private http: HttpClient){}

    // getShoes method returns entire list of data(shoes
    getShoes(): Observable<IShoe[]>{

        return this.http.get<IShoe[]>(this.shoeUrl).pipe(
            tap(items => console.log(JSON.stringify(items))),
            catchError(this.handleError)
        );
    }

    getShoe(id:number): Observable<IShoe | undefined>{
        return this.getShoes()
                .pipe(
                    map((shoes:IShoe[]) => shoes.find(s => s.id === id)));
    }

    private handleError(error: HttpErrorResponse){

        let errorMsg = '';

        if(error.error instanceof ErrorEvent){
            errorMsg = `An error happened: ${error.error.message}`;
        }else{
            errorMsg = `Server returned code: ${error.status}, error message: ${error.message}`;
        }
        console.error(errorMsg);
        return throwError(() => new Error('test'));
    }
}