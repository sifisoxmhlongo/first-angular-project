import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShoeListComponent } from './shoe-list.component';
import { ShoeDetailsComponent } from './shoe-details.component';
import { ConvertToSpace } from '../shared/convert-char-to-space.pipe';
import { RouterModule } from '@angular/router';
import { ShoeDetailGuard } from './shoe-detail.guard';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ShoeListComponent,
    ShoeDetailsComponent,
    ConvertToSpace
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild([
      { path: 'shoes', component: ShoeListComponent},
      { 
        path: 'shoes/:id', 
        canActivate: [ShoeDetailGuard],
        component: ShoeDetailsComponent 
      },
    ]),
    SharedModule
  ]
})
export class ShoeModule { }
