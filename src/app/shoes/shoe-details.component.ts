import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IShoe } from './IShoe';
import { ShoeListComponent } from './shoe-list.component';
import { ShoeService } from './shoe.service';
import { Subscription } from 'rxjs';

@Component({
  templateUrl: './shoe-details.component.html',
  styleUrls: ['./shoe-details.component.css']
})
export class ShoeDetailsComponent implements OnInit{

  errorMessage = '';
  pageTitle: string = 'Shoe brand'
  shoeList: IShoe[] =[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private shoeService: ShoeService       
            ){}

  shoe: IShoe | undefined;

  ngOnInit(): void {

    const id = Number(this.route.snapshot.paramMap.get('id'));

    if (id) {
      this.getShoe(id);
    }
}



  onBack(): void {
    this.router.navigate(['/shoes']);
  }

  getShoe(id: number): void {
    this.shoeService.getShoe(id).subscribe({
      next: item => this.shoe = item,
      error: err => this.errorMessage = err
    });
  }
}
