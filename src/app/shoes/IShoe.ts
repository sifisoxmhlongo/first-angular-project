export interface IShoe{
    brand: string;
    color: string;
    size: number;
    rating: number;
    imageUrl: string;
    id:number;
}