import { Component, EventEmitter, OnDestroy, OnInit, Output } from "@angular/core";
import { IShoe } from "./IShoe";
import { ConvertToSpace } from "../shared/convert-char-to-space.pipe";
import { ShoeService } from "./shoe.service";
import { Subscription } from "rxjs";

@Component({
    templateUrl: './shoe-list.component.html',
    styleUrls: ['./shoe-list.component.css']

})
export class ShoeListComponent implements OnInit, OnDestroy {
    
    componentTitle: string = 'Shoe Table';
    showImage: boolean = true;

    shoes:IShoe[]= [];
    filteredList: IShoe[] = [];

    private _filteredString: string="";

    imageWidth: number = 100;
    imageMargin: number = 2;

    errorMessage: string = '';
    sub!: Subscription;

    constructor(private shoeService: ShoeService){}

    ngOnDestroy(): void {
        this.sub.unsubscribe();
    }

    ngOnInit(): void {
        this.sub = this.shoeService.getShoes().subscribe({
            next: data => {
                this.shoes = data;
                this.filteredList = this.shoes;
            },
            error: err => this.errorMessage = err
        });
    }

    onRatingClickEvent(msg: number): void{

        if(msg == 1){
            this.componentTitle =  "Old Nike sneaker with rating of " + msg;
        }  

        if(msg == 5){
            this.componentTitle =  "Stylish Nok sneaker with rating of " + msg;
        }  

        if(msg == 3){
            this.componentTitle =  "Latest Yeezy sneaker with rating of " + msg;
        }       
    }

    toggleImage():void{
        this.showImage = !this.showImage;
    }

    get filteredString(): string {
        return this._filteredString;
    }

    set filteredString(value: string){
        this._filteredString = value;
        console.log("set value is: ", value);
        this.filteredList = this.performFiltering(value);
    }

    performFiltering(value: string): IShoe[] {

        value =value.toLocaleLowerCase();
        return this.shoes.filter((shoe:IShoe) =>
            shoe.brand.toLocaleLowerCase().includes(value));
    }

}